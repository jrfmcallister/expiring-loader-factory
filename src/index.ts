import DataLoader from "dataloader";
import { LRUMap } from "lru_map";

type Wrapped<Value> = {
  value: Value;
  expiry_epoch_seconds: number;
};

type Settings = {
  ttl_seconds: number;
  lru_limit: number;
};

export function factory<Key, Value>(
  loader: (key: Key) => Promise<Value>,
  settings: Settings
): (key: Key) => Promise<Value> {
  const wrappedLoader = async (key: Key): Promise<Wrapped<Value>> =>
    wrappedResp(await loader(key), settings);

  const wrappedDataLoader = dataLoader(wrappedLoader, settings);

  return async (key) => {
    const wrapped = await wrappedDataLoader.load(key);

    const current_epoch_seconds = new Date().getTime() / 1000;
    if (wrapped.expiry_epoch_seconds < current_epoch_seconds) {
      wrappedDataLoader.clear(key);
      const newWrapped = await wrappedDataLoader.load(key);
      return newWrapped.value;
    } else {
      return wrapped.value;
    }
  };
}

const dataLoader = <Key, Value>(
  loader: (key: Key) => Promise<Value>,
  settings: Settings
) => {
  const batchLoader: DataLoader.BatchLoadFn<Key, Value> = (keys) =>
    Promise.all(keys.map(loader));
  return new DataLoader(batchLoader, {
    cacheMap: new LRUMap(settings.lru_limit),
  });
};

const wrappedResp = <Value>(
  value: Value,
  settings: Settings
): Wrapped<Value> => {
  const current_epoch_seconds = new Date().getTime() / 1000;
  return {
    value: value,
    expiry_epoch_seconds: settings.ttl_seconds + current_epoch_seconds,
  };
};
