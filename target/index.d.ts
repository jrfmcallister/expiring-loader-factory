export declare function expiringLoaderFactory<Key, Value>(loader: (key: Key) => Promise<Value>, ttl_seconds: number): (key: Key) => Promise<Value>;
